#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 20 15:23:11 2021

@author: danpal
"""

import sys

import plotext as plt

VALID_FORMAT_CHARS = {"x", "g"}


def parse_format(format_):
    format_ = format_.lower()
    format_set = set(format_)
    if "x" not in format_set:
        raise ValueError('Must use the "x" character in the format')
    if not format_set <= VALID_FORMAT_CHARS:
        raise ValueError(
            f"Invalid format characters: {format_set - VALID_FORMAT_CHARS}"
        )
    if len(format_set) < len(format_):
        raise ValueError("Must specify each format character only once")
    return {c: i for i, c in enumerate(format_)}


def parse_input(input_, delimiter="\t", format_="y"):
    groups = {}
    format_ = parse_format(format_)
    for line in input_:
        if line:
            row = line.split(delimiter)
            group = row[format_["g"]].strip() if "g" in format_ else None
            x = float(row[format_["x"]]) if "x" in format_ else None
            groups.setdefault(group, []).append(x)
    return groups


def plot_data(data, bins=10):
    for group, values in data.items():
        plt.hist(values, label=group, bins=bins)
    plt.show()


if __name__ == "__main__":
    from argparse import ArgumentParser

    def parse_args():
        parser = ArgumentParser()
        parser.add_argument("-d", "--delimiter", nargs="?", default="\t")
        parser.add_argument("-f", "--format", nargs="?", default="x")
        parser.add_argument("-b", "--bins", nargs="?", default=10, type=int)
        parser.add_argument("input", nargs="?", metavar="FILE", default="-")
        return parser.parse_args()

    def main():
        args = parse_args()
        if args.input == "-":
            input_ = sys.stdin.readlines()
        else:
            with open(args.input) as file:
                input_ = file.readlines()
        data = parse_input(
            input_, delimiter=args.delimiter, format_=args.format
        )
        plot_data(data, bins=args.bins)

    main()
