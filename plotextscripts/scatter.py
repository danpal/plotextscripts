#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 20 15:23:11 2021

@author: danpal
"""

import sys

import plotext as plt

VALID_FORMAT_CHARS = {"x", "y", "g"}


def parse_format(format_):
    format_ = format_.lower()
    format_set = set(format_)
    if "y" not in format_set:
        raise ValueError('Must use the "y" character in the format')
    if not format_set <= VALID_FORMAT_CHARS:
        raise ValueError(
            f"Invalid format characters: {format_set - VALID_FORMAT_CHARS}"
        )
    if len(format_set) < len(format_):
        raise ValueError("Must specify each format character only once")
    return {c: i for i, c in enumerate(format_)}


def parse_input(input_, delimiter="\t", format_="y"):
    groups = {}
    format_ = parse_format(format_)
    for line in input_:
        if line:
            row = line.split(delimiter)
            group = row[format_["g"]].strip() if "g" in format_ else None
            x = float(row[format_["x"]]) if "x" in format_ else None
            y = float(row[format_["y"]]) if "y" in format_ else None
            if x is not None:
                x_data, y_data = groups.setdefault(group, ([], []))
                x_data.append(x)
                y_data.append(y)
            else:
                (y_data,) = groups.setdefault(group, ([],))
                y_data.append(y)
    return groups


def plot_data(data):
    for group, values in data.items():
        plt.scatter(*values, label=group)
    plt.show()


if __name__ == "__main__":
    from argparse import ArgumentParser

    def parse_args():
        parser = ArgumentParser()
        parser.add_argument("-d", "--delimiter", nargs="?", default="\t")
        parser.add_argument("-f", "--format", nargs="?", default="y")
        parser.add_argument("input", nargs="?", metavar="FILE", default="-")
        return parser.parse_args()

    def main():
        args = parse_args()
        if args.input == "-":
            input_ = sys.stdin.readlines()
        else:
            with open(args.input) as file:
                input_ = file.readlines()
        data = parse_input(
            input_, delimiter=args.delimiter, format_=args.format
        )
        plot_data(data)

    main()
